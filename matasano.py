#Q1
A="49276d206b696c6c696e6720796f757220627261696e206c696b65206120706f69736f6e6f7573206d757368726f6f6d".decode("hex")
A=A.encode("base64")
print(A)

#Q2

B="1c0111001f010100061a024b53535009181c".decode("hex")
C="686974207468652062756c6c277320657965".decode("hex")
def xorz(X, Y):
    M,N=[ord(i) for i in X], [ord(i) for i in Y]
    return "".join([chr(M[i]^N[i]) for i in range(len(X))])
print(xorz(B,C).encode('hex'))
print("\n")

#Q3

A='1b37373331363f78151b7f2b783431333d78397828372d363c78373e783a393b3736'.decode('hex')
B=[]
for i in range(26):
    B.append([chr(97+i), xorz(A, chr(97+i)*len(A))])
    B.append([chr(65+i), xorz(A, chr(65+i)*len(A))])

#==========
from freqfuncts import *
for [i,j] in B:
    if checkFreq(j, .17) == 1:
        print i, j, checkFreq(j, .17)
print('\n\n')

#okay, the above method works only insofar as it reduces the number of strings that I have to look at... So below is a dictionary method
#==========


import os
os.chdir('/home/nan/crypt/')
words = [i for i in open('wlist/wlist_match3.txt').read().split('\n')]
words.pop()
sepwords = [list() for i in range(26)]
for i in words:
    if 'a'<=i[0]<='z':
        sepwords[ord(i[0])-97].append(i)

def checkWords(LOL): #for string of words>=0
    words=0
    for i in LOL.split():
        if i.isalpha() and ((len(i)>1 and i[0]!=i[1]) or i.lower()=='a')  and (i.islower() or i.isupper() or i.istitle()):
            for j in sepwords[ord(i[0])- (i[0].isupper())*65 - (i[0].islower())*97]:
                if i.lower() == j.lower():
                    words+=1
                    break
    return [words, len(LOL.split())]

for i in B:
    if checkWords(i[1])[0] > 0: print(i[1], '(words: ',checkWords(i[1])[0], ' out of ',checkWords(i[1]))


#Q4

Hexes = [i.strip() for i in open("hexes.txt").readlines()]
Fload = []#list of words by letter
for i in Hexes:
    for case in (65, 97): #we gonna test all dem chars :D
        for j in range(26):
            Fload.append([chr(case+j), xorz(i, chr(case+j)*len(Hexes))])
guesses = []

'''for i in range(len(Fload)): 
    if checkWords(Fload[i][1])[0] > 0:
        print (i, Fload[i][0])
        print checkWords(Fload[i][1]), Fload[i][1]
        guesses.append([i, Fload[i][1], Fload[i][0]])
for i in guesses:
    print i[1]
    print i[2]
'''
#Q5
vanilla, key= "Burning 'em, if you ain't quick and nimble\nI go crazy when I hear a cymbal", "ICE"
VL,KL=len(vanilla), len(key)
repkey=key*(VL/KL) + "".join([key[i] for i in range(VL%KL)])
print(xorz(vanilla, repkey).encode('hex'))
#Q6

keysize=2

rap='this is a test'
brap='wokka wokka!!!'
def hamming(H,G):
    dist=0
    if (type(H) and type(G))==type('a'):
        for i,j in zip(H,G):
            dist+= sum([k=='1' for k in str(bin(ord(i)^ord(j)))])
    elif (type(H) and type(G))==type(1):
        return sum([k=='1' for k in str(bin(H^G))])
    return dist
print hamming(rap, brap)

hammingz=[sum([hamming(i,j) for j in (i, i+j)]) for i in range(2, 41)]

#Q7

from Crypto.Cipher import AES
import base64
B64=("".join([i for i in open('B64.txt').readlines()])).decode('base64')
ci=AES.new("YELLOW SUBMARINE")
print ci.decrypt(B64)
file('B64.txt').close()

#Q8

ECBs = [(i.rstrip()).decode('hex') for i in open('Q8HEX.txt').readlines()]
for i in ECBs:
    if checkWords(ci.decrypt(i))[0] > 0:
        print ci.decrypt(i)
file('Q8HEX.txt').close()


#Q00

lol = "Damn, you're optimistic, Smari"
print (xorz(lol, 'L'*len(lol)).encode('base64')).encode('hex')
print
a='43433068496d42734e534d35617a347062434d384f4355684a5438344a533967624238684c54346c0a'
lolol=(a.decode('hex')).decode('base64')
print xorz(lolol, 'L'*len(lolol))
#http://thepiratebay.se/torrent/5667360/Samurai_Swordsmanship_by_Masayuki_Shimabukuro___Bonus_ebook_on_p